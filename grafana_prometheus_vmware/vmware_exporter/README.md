# VMware exporter в Docker

VMware vCenter Exporter для Prometheus.

Exporter предназначен для получения информации о хостах vCenter:
- Метрики виртуальных машин, esxi, cluster и т.д.

## Использование
### Сборка образа

```
docker build -t skensel/vmware_exporter:0.13.2 .
```



### Переменные окружения
| Variable                      	      | Defaults  | Description                                                            |                                      	
| --------------------------------------| --------- | -----------------------------------------------------------------------|
| `VSPHERE_HOST`               		      |  n/a      | IP Vsphere сервера  						                                       |
| `VSPHERE_USER`               		      |  n/a      | Пользователь для подключения к Vsphere 						                     |
| `VSPHERE_PASSWORD`           		      |  n/a      | Пароль для подключения к Vsphere  					                           |
| `VSPHERE_SPECS_SIZE`         		      |  5000     | Размер списка спецификаций для функции статистики запроса 			       |
| `VSPHERE_IGNORE_SSL`         		      |  False    | Игнорировать сертификат ssl при подключении к хосту vsphere 			     |



### Пример в docker-compose
```
version: "3"
services:
  vmware_exporter:
    image: skensel/vmware_exporter:0.13.2
    ports:
      - 9272:9272
    env_file:
      - env/efk_environment.env
```